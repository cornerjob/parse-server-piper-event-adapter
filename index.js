'use strict';

const KinesisService = require('./services/kinesis/KinesisService');
const TYPE_COMPANY = 'company';
const TYPE_CANDIDATE = 'candidate';

const sendMail = options => {
  return;
};

const PiperEventAdapter = () => {
  const sendVerificationEmail = options => {
    const userId = options.user.id ? options.user.id : options.user.get('saveBridge').userId;

    if (!userId) {
      return;
    }

    let type;
    const isNewUser = options.user.has('createdAt') ?
      options.user.get('createdAt').getTime() === options.user.get('updatedAt').getTime() : false;

    if (options.user.get('type') === TYPE_CANDIDATE && isNewUser) {
      return;
    }

    if (options.user.get('type') === TYPE_COMPANY) {
      type = 'IdentityCompanyVerificationEmailRequested';
    } else {
      type = 'IdentityCandidateVerificationEmailRequested';
    }

    const dataS3 = {
      '@context': 'http:\/\/www.w3.org\/ns\/activitystreams',
      type,
      actor: {
        userId
      },
      provider: {
        type: 'Parse'
      },
      object: {
        userId,
        isNewUser,
        userEmail: options.user.get('email'),
        userName: options.user.get('firstName'),
        userType: options.user.get('type'),
        verificationLink: options.link
      }
    };

    return KinesisService.setS3Stream(type, dataS3);
  };

  const sendPasswordResetEmail = options => {

    const dataS3 = {
      '@context': 'http:\/\/www.w3.org\/ns\/activitystreams',
      provider: {
        type: 'Parse'
      },
      actor: {
        userId: options.user.id
      },
      object: {
        userEmail: options.user.get('email'),
        userName: options.user.get('firstName'),
        resetLink: options.link
      }
    };

    return KinesisService.setS3Stream('UserCallResetPassword', dataS3);
  };

  return Object.freeze({
    sendPasswordResetEmail,
    sendVerificationEmail,
    sendMail
  });
}

module.exports = PiperEventAdapter;
