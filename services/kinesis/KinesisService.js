'use strict';

const request = require('superagent');

const {
  PIPER_URL
} = process.env;

class KinesisService {

  /**
   * @param event
   * @param data
   *
   */
  setS3Stream(event, data) {
    return new Promise((resolve, reject) => {
      request
        .post(`${PIPER_URL}/${event}`)
        .send(data)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          if (err) {
            reject(err.response);
          } else {
            resolve(res.body);
          }
        });
    });
  }
}

module.exports = new KinesisService();
