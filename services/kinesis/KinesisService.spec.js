/* eslint-env mocha */
'use strict';

const proxyquire =  require('proxyquire');
const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;
const sinon = require('sinon');
require('sinon-as-promised');

let postStub = sinon.stub().returnsThis(),
  sendStub = sinon.stub().returnsThis(),
  setStub = sinon.stub().returnsThis(),
  responseStub,
  errorStub,
  endStub = cb => {
    cb(errorStub, responseStub);
    return this;
  },
  superagentMock = {
    post: postStub,
    send: sendStub,
    set: setStub,
    end: endStub
  };

const piperUrl = 'test',
  KinesisService = proxyquire('./KinesisService', {
    'superagent': superagentMock,
    '../../lib/configServer': {
      piperUrl: piperUrl
    }
  });

/**
 * Test KinesisService service call
 */
describe('KinesisService', () => {
  it('should call kinesis service with the right parameters', () => {
    const event = 'test',
      data = 'test';

    errorStub = null;
    responseStub = {
      body: 'test'
    };

    postStub = sinon.stub().withArgs(`${piperUrl}/${event}`).returnsThis();
    sendStub = sinon.stub().withArgs(data).returnsThis();
    return expect(KinesisService.setS3Stream(event, data)).to.become(responseStub.body);
  });

  it('should throw an error when service returns an error', () => {
    const event = 'test',
      data = 'test';

    errorStub = {
      response: {
        body: {
          error: 'test'
        }
      }
    };

    postStub = sinon.stub().withArgs(`${piperUrl}/${event}`).returnsThis();
    sendStub = sinon.stub().withArgs(data).returnsThis();

    return expect(KinesisService.setS3Stream(event, data)).to.rejectedWith(errorStub.response.body);
  });
});
